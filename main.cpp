/* gltut.c
 *
 * Copyright 2018 Binyamin Darshan Hcohen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#define GLEW_STATIC

#include<GL/glew.h>

#include<iostream>
#include<SDL2/SDL.h>
#include<SDL2/SDL_opengl.h>

using namespace std;

const char* vertexSource = R"glsl(
    #version 150 core

    in vec2 position;

    void main()
    {
        gl_Position = vec4(position, 0.0, 1.0);
    }
)glsl";

const char *fragmentSource = R"glsl(
	#version 150 core

	out vec4 outColor;

	void main()
	{
    	outColor = vec4(1.0, 1.0, 1.0, 1.0);
	}
)glsl";

int main (int argc, char *argv[])
{
	//initialize
	SDL_Init(SDL_INIT_VIDEO);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	
	SDL_Window *win = SDL_CreateWindow("GlTut", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_OPENGL);
	SDL_GLContext context = SDL_GL_CreateContext(win);
	
	glewExperimental = GL_TRUE;
	glewInit();
	
	GLuint vo;
	glGenVertexArrays(1, &vo);
	glBindVertexArray(vo);
	//shape to draw
	float vert[] = {
		0.0f, 0.5f,
		0.5f, -0.5f,
		-0.5f, -0.5f
	};
	
	//loading the shape to GPU memory
	GLuint v;
	glGenBuffers(1, &v);
	glBindBuffer(GL_ARRAY_BUFFER, v);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vert), vert, GL_STATIC_DRAW);
	
	//Loading vertex shader to GPU memory
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexSource, NULL);
	glCompileShader(vertexShader);
	GLint status;
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
	if(status == GL_TRUE) cout << "vertex shader generated" << endl;
	
	//Loading Fragment shader to GPU memory
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
	if(status == GL_TRUE) cout << "fragment shader generated" << endl;
	
	//linking the program with the shaders
	GLuint program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);
	glUseProgram(program);
	
	GLint posAttr = glGetAttribLocation(program, "position");
	glVertexAttribPointer(posAttr, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(posAttr);
	
	
	
	
	SDL_Event e;
	for(bool running = true;running;)
	{
		while(SDL_PollEvent(&e)) if(e.type==SDL_QUIT) running = false;
		
		glDrawArrays(GL_TRIANGLES, 0, 3);
		SDL_GL_SwapWindow(win);
	}
	
	
	SDL_GL_DeleteContext(context);
	SDL_Quit();
	return 0;
}
