all: gltut

WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O2

gltut: Makefile gltut.c
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) gltut.c

clean:
	rm -f gltut

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	./gltut

